require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'net/http'
require 'mechanize'
require 'fileutils'

urls=[
	'http://www.saksfifthavenue.com/main/ProductArray.jsp?FOLDER%3C%3Efolder_id=2534374306431457&use_parent=1&SECSLOT=LN-Gloves+%26+Hats&bmUID=iZa91EV&N=306431457',
	'http://www.saksfifthavenue.com/main/ProductArray.jsp?FOLDER%3C%3Efolder_id=2534374306431457&use_parent=1&Nao=60&SECSLOT=LN-Gloves+%26+Hats&N=306431457&bmUID=iZa91EV',
	'',
	'',
	'',
	'',
	'',
	'',
	'',
	'',
	'',
	'',
	''
]
SITE_BASE="http://www.saksfifthavenue.com"
#given a url fetch all image urls
def fetch_images(url,recurse=false)
	puts "###########{url}#############"
	agent = Mechanize.new
	agent.get(url)
	agent.page.forms[1].submit
	image_page=agent.get(url) #get again
	#first find the brand
	brand=image_page.at("#leftNavBrands option[@selected='yes']")
	brand_name="others"
	if brand
		brand_name= brand.text.squeeze(" ").sub(" ","_")
	end
	dir_name="download_images/#{brand_name}"
	if !File.directory? dir_name
		FileUtils.mkdir_p dir_name
	end
	image_urls=image_page.search("img.pa-product-large","img.alt-image-large").map {|node| {:id=>node[:id],:src=>node[:src] }}
	image_urls.each do |image_node|
		#save the image to disk
		#check if the image exist
		image_full_path="#{dir_name}/#{image_node[:id]}.jpg"
		if File.exist? image_full_path
			puts "#{image_full_path} exist...^^^^^^"
		else
			open(image_full_path,'wb') do |file|
				file<<open(image_node[:src]).read
				puts "#{image_full_path} ===== saved ok ======="
			end
		end
	end
	#check if this page contains other pages
	if recurse
		#get other page urls
		page_links=image_page.search("#pc-bottom ol.pa-page-number li a")
		if page_links.count>0
			puts "________find another #{page_links.count} pages,starting get those images........."
			page_links.each do |page_link|
				if page_link[:href]
					fetch_images("#{SITE_BASE}#{page_link[:href]}")
				end
			end
		end
	end
end
urls.each do |url|
	fetch_images(url,true) if url!=""
end
#doc = Nokogiri::HTML(open(URL))
#puts doc.content
#open('image.html', 'wb') do |file|
#  file << open(URL).read
#end
#images=fetch_images(URL)
#puts images.count

