require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'net/http'
require 'mechanize'
require 'fileutils'

urls=[
]
SITE_BASE="http://www.style.com/"
#fetch fashion shows
def fetch_shows(show_url)
	#get all brands
	doc=Nokogiri::HTML(open(show_url))
	show_details = doc.css("ol#all_shows_list li a").map do |link|
		special_char_index=link[:href].index "-"
		{:name=>link.text,:url=>link[:href],:short_name=>link[:href][(special_char_index+1)..-1] }
	end
end

def fetch_fs_images_by_brand(category,brand_short_name)
	image_format="http://www.style.com/slideshows/2010/fashionshows/#{category}/#{brand_short_name}/RUNWAY/"
	#check directory
	full_dir_path="download_images/style.com/#{category}/#{brand_short_name}"
	if !File.directory? full_dir_path
		FileUtils.mkdir_p full_dir_path
	end
	total_num = get_total_image_num(category,brand_short_name)
	total_num.times do |num|
		puts "getting ..... #{num+1} of #{brand_short_name}"
		image_name="#{(num+1).to_s.rjust(4,'0')}0fullscreen.jpg"
		full_url="#{image_format}#{image_name}"
		#check if the file is exist
		if File.exist? "#{full_dir_path}/#{image_name}"
			puts "#{full_dir_path}/#{image_name}   is exist+++++"
			next
		end
		begin
			open("#{full_dir_path}/#{image_name}","wb") do |file|
				puts "start downloading image #{image_name}~~~~~~~~~"
				file<<open(full_url).read
				puts "#{image_name} saved....."
			end
		rescue
			puts "!!!!  #{full_dir_path}/#{image_name} cannot download!!!!!!! "
		end
	end
end

def get_total_image_num(category,brand_short_name)
	puts "geting the total image num of #{brand_short_name}..."
	url="http://www.style.com/fashionshows/complete/#{category}-#{brand_short_name}"
	doc=Nokogiri::HTML(open(url))
	page_spans=doc.css("#thumbs_nav span").select {|span| span.text.include? "images"}
	page_text=page_spans.first.text
	of_index=page_text.index "of"
	total_num=page_text[(of_index+2)..-1].to_i
	puts "totol image num is #{total_num}"
	total_num
end
def url_exist?(url)
	begin
		open(url).status==["200","OK"]
	rescue
		puts "#{url} not exist ....."
		false 
	end
end

#the season name is used to create directory
#the show url is to retrive images
def fetch_2011_shows(season_name,show_url)
	puts "....................begin get show names......"
	show_details=fetch_shows show_url
	puts "get shows total count is #{show_details.count}"
	puts "....................done..."
	puts "begin retrive images.........."
	show_details.each do |show|
		puts "***********begin retrive #{show} 's images**********************"
		fetch_fs_images_by_brand season_name,show[:short_name]
		puts "***********retrive #{show} 's images done***********************"
	end
end
puts fetch_2011_shows("S2011RTW","http://www.style.com/fashionshows/collections/S2011RTW/")
#http://www.style.com/slideshows/2011/fashionshows/F2011RTW/ADAMEVE/RUNWAY/00020fullscreen.jpg

